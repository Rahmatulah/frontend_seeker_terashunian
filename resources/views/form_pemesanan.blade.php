@extends('layouts.main2')
@section('title', 'Form Pembelian')
<link rel="stylesheet" href="{{ asset('css/form_pemesanan.css') }}">
@section('content')
<div class="container my-5">

    <div class="row">

        <main class="col-sm-12 col-md-12 col-lg-12 col-xl-12" role="main">

            {{-- header breadcumb --}}
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                        <h6 class="text-secondary d-flex justify-content-start">
                            <i>
                                {{ request()->is('pemesanan')? "Beranda / Pencarian / Detail / Form Pembelian" : '' }}
                            </i>
                        </h6>
                    </div>
                </div>
            {{--  --}}

            {{-- form pemesanan --}}
            <div class="card card-form">
                <div class="card-body">
                    <div class="d-flex align-items-start">
                        <img src="{{ asset('images/gamvar-thumb.png') }}" alt="Gambar Thumb">
                    </div>
                    <div class="d-flex align-items-end col-sm-12 col-md">
                        <form class="form-inline">
                            <div class="form-group mb-2">
                              <label for="staticEmail2" class="sr-only">Email</label>
                              <input type="text" readonly class="form-control-plaintext" id="staticEmail2" value="email@example.com">
                            </div>
                            <div class="form-group mx-sm-3 mb-2">
                              <label for="inputPassword2" class="sr-only">Password</label>
                              <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Confirm identity</button>
                        </form>
                    </div>
                </div>
            </div>
            {{-- akhir form pemesanan --}}

        </main>

    </div>

</div>
@endsection