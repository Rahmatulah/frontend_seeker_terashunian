@extends('layouts.main')
@section('title', 'Detail Informasi')
<link rel="stylesheet" href="{{ asset('css/detail-style.css') }}">
<style>
    *{
        margin-top: 0;
        padding-top: 0;
        padding-bottom: 0;
    }
</style>
@section('content')
    <div class="container my-3">

        <div class="row">
            <main class="col-sm-12 col-md-12 col-lg-12 col-xl-12" role="main">
                
                {{-- header breadcumb --}}
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
                            <h6 class="text-secondary d-flex justify-content-start">
                                <i>
                                    {{ request()->is('detail')? "Beranda / Pencarian / Detail" : '' }}
                                </i>
                            </h6>
                        </div>
                        <div class="col-m-6 col-md-6 col-lg-6 col-xl-6">
                            <h6 class="text-secondary d-flex justify-content-end" style="font-size:12px;">
                                <i class="fas fa-history" style=" -moz-transform: rotate(-50deg);-webkit-transform: rotate(-50deg); -ms-transform: rotate(-50deg);-o-transform: rotate(-50deg);
                                "></i>&nbsp;&nbsp;Diupload 4 Minggu Lalu
                            </h6>
                        </div>
                    </div>
                {{--  --}}

                {{-- Carousel --}}
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            {{-- slider detail informasi rumah --}}
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel1.jpg" alt="First slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel1.jpg" alt="Second slide">
                                </div>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel1.jpg" alt="Third slide">
                                </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                                </a>
                            </div>
                            {{-- akhir slider detail --}}
                        </div>
                    </div>
                {{--  --}}

                {{-- Judul dan tombol pesan sekarang --}}
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-8 col-xl-8">
                            <h1>
                                Judul Rumah Real Estate
                            </h1>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-end">
                            <h6>
                                <i class="fas fa-heart px-2" id="like-heart"></i>
                                <i class="fas fa-share-alt px-2" id="bagikan"></i>
                                <a href="#" class="btn btn-pesan-sekarang">Pesan Sekarang</a>
                            </h6>
                        </div>
                    </div>
                {{-- akhir Judul dan tombol pesan sekarang --}}

                {{-- harga dan detail informasi--}}
                    <div class="row">

                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="card detail-informasi">

                                <div class="card-header" style="background-color: white;">
                                    <h4 class="harga">
                                        Rp 950 jt
                                    </h4>
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-sm-12 col-md-7 col-xl-7 col-lg-7">
                                            <h5 class="card-title mb-3" style="font-weight: 600;">
                                                Detail Informasi
                                            </h5>
                                            <h6 class="card-subtitle mb-3">
                                                <span class="pr-3">
                                                    LT&nbsp;&nbsp;:&nbsp;&nbsp;120 m<sup>2</sup>
                                                </span>
                                                <span>
                                                    LB&nbsp;&nbsp;:&nbsp;&nbsp;120 m<sup>2</sup>
                                                </span>
                                            </h6>
                                            <p>
                                                <span class="pr-3">
                                                    <img src="{{ asset('images/bedroom.svg') }}" alt="Bed Room"><span>&nbsp;&nbsp;2</span>
                                                </span>
                                                <span class="pr-3">
                                                    <img src="{{ asset('images/bathroom.svg') }}" alt="Bath Room"><span>&nbsp;&nbsp;2</span>
                                                </span>
                                                <span class="pr-3">
                                                    <img src="{{ asset('images/kitchenroom.svg') }}" alt="Kitchen Room"><span>&nbsp;&nbsp;2</span>
                                                </span>
                                                <span class="pr-3">
                                                    <img src="{{ asset('images/dinningroom.svg') }}" alt="Dinning Room"><span>&nbsp;&nbsp;2</span>
                                                </span>
                                                <span class="pr-3">
                                                    <img src="{{ asset('images/garageroom.svg') }}" alt="Garage Room"><span>&nbsp;&nbsp;2</span>
                                                </span>
                                            </p>
                                            <p class="text-justify" style="font-size:14px;">
                                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci quidem numquam qui officia tempora nostrum ipsa fuga, atque quibusdam aliquam ut, in aperiam animi quia voluptatum? Expedita vitae vero dicta!Molestias ipsa, numquam distinctio, ab aperiam doloribus voluptatem fugit reprehenderit voluptates nulla voluptatibus praesentium laborum fuga modi. Enim, quisquam fugiat veniam quos non possimus et, repellat itaque vel similique mollitia?
                                            </p>
                                            <table>
                                                <tr>
                                                    <td width="45%">
                                                        Sertifikat
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        SHM(Sertifikat Hak Milik)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Asset
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        Bank BRI
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Harga per m<sup>2</sup>
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        Rp 20,000,000
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Tipe Rumah
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        170
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Interior
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        Vintage
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Jumlah Lantai
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        2
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Air
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        tandar PDAM
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="45%">
                                                        Listrik
                                                    </td>
                                                    <td width="2%">
                                                        <b>:</b>
                                                    </td>
                                                    <td width="53%">
                                                        2200 Watt
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5 d-flext justify-content-end">
                                            <iframe id="frame-map-detail" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.915516821555!2d106.81957221434232!3d-6.404885064422656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69ebdeef971301%3A0x7f3ce8c7e605b066!2s3%2C%20Jl.%20Belimbing%203%20No.29%2C%20RW.1%2C%20Depok%2C%20Kec.%20Pancoran%20Mas%2C%20Kota%20Depok%2C%20Jawa%20Barat%2016431!5e0!3m2!1sid!2sid!4v1608618954179!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;"  aria-hidden="false" tabindex="0"></iframe>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                {{-- akhir harga dan detail informasi--}}

                {{-- Iklan --}}
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div  style="height: 150px;background-color:#333;" width="100%">
                                <h2 class="text-center text-secondary">
                                    IKLAN
                                </h2>
                            </div>
                        </div>
                    </div>
                {{--  --}}

            </main>
        </div>

        
            
    </div>
@endsection