@extends('layouts.main')
@section('title', 'Home')
<link rel="stylesheet" href="{{ asset('css/home-style.css') }}">
@section('content')

    {{-- SLider --}}
    <div class="slider">
      <div class="owl-carousel owl-theme" style="">
        <div class="owl-slide d-flex align-items-center cover" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel1.jpg);height: 500px !important;">
          <div class="container">
            <div class="row justify-content-center justify-content-md-start">
              <div class="col-10 col-md-6 static">
                <div class="owl-slide-text">
                  <h2 class="owl-slide-animated owl-slide-title">The bedding was hardly able to cover it</h2>
                  <div class="owl-slide-animated owl-slide-subtitle mb-3">
                    One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin.
                  </div>
                  <a class="btn btn-primary owl-slide-animated owl-slide-cta" href="https://unsplash.com/photos/pgR4yBMjum8" target="_blank" role="button">See Original Image</a>
                </div>
              </div>
            </div>
          </div>
        </div><!--/owl-slide-->
      
        <div class="owl-slide d-flex align-items-center cover" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg);height: 500px !important;">
          <div class="container">
            <div class="row justify-content-center justify-content-md-start">
              <div class="col-10 col-md-6 static">
                <div class="owl-slide-text">
                  <h2 class="owl-slide-animated owl-slide-title">Samsa was a travelling salesman.</h2>
                  <div class="owl-slide-animated owl-slide-subtitle mb-3">
                    It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather.
                  </div>
                  <a class="btn btn-primary owl-slide-animated owl-slide-cta" href="https://unsplash.com/photos/Ijx8OxvKrgM" target="_blank" role="button">See Original Image</a>
                </div>
              </div>
            </div>
          </div>
        </div><!--/owl-slide-->
      
        <div class="owl-slide d-flex align-items-center cover" style="background-image: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel3.jpg);height: 500px !important;">
          <div class="container">
            <div class="row justify-content-center justify-content-md-start">
              <div class="col-10 col-md-6 static">
                <div class="owl-slide-text">
                  <h2 class="owl-slide-animated owl-slide-title">A collection of textile samples.</h2>
                  <div class="owl-slide-animated owl-slide-subtitle mb-3">
                    He lay on his armour-like back, and if he lifted his head a little he could see his brown belly.
                  </div>
                  <a class="btn btn-primary owl-slide-animated owl-slide-cta" href="https://unsplash.com/photos/7uwbhGSH5Fg" target="_blank" role="button">See Original Image</a>
                </div>
              </div>
            </div>
          </div>
        </div><!--/owl-slide-->
      </div>
    </div>
    {{-- End Of Silider --}}

    <div class="recomendation">

      <div class="container py-4">

        <div class="row">
          <div class="col-sm-6 col-md-8 col-lg-8 col-xl-8">
            <h3 class="d-flex justify-content-start">
              Rekomendasi Baru
            </h3>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 col-xl-4">
            <h5 class="d-flex justify-content-end lihat-lebih-banyak">
              <a href="#">
                <span style="color:#8B8530;font-weight:500;">Lihat Lebih Banyak</span>
              </a>
            </h5>
          </div>
        </div>

        <div class="row">

          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 my-2">
            <div class="card card-rekomendasi" >
              <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
              <div class="card-body pt-0">
                <h5 class="card-title">
                    <a href="#" target="_blank" rel="noopener noreferrer" >
                      <span style="color:#333;">Judul Real Estate</span>
                    </a>
                </h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 my-2">
            <div class="card card-rekomendasi" >
              <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
              <div class="card-body pt-0">
                <h5 class="card-title">
                    <a href="#" target="_blank" rel="noopener noreferrer">
                      <span style="color:#333;">Judul Real Estate</span>
                    </a>
                </h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 my-2">
            <div class="card card-rekomendasi" >
              <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
              <div class="card-body pt-0">
                <h5 class="card-title">
                    <a href="#" target="_blank" rel="noopener noreferrer">
                      <span style="color:#333;">Judul Real Estate</span>
                    </a>
                </h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              </div>
            </div>
          </div>

          <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 my-2">
            <div class="card card-rekomendasi" >
              <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
              <div class="card-body pt-0">
                <h5 class="card-title">
                    <a href="#" target="_blank" rel="noopener noreferrer">
                      <span style="color:#333;">Judul Real Estate</span>
                    </a>
                </h5>
                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>

    {{-- Tentang Kami --}}
    <div class="about-us py-4">
      <div class="container">

        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h3 class="text-center">
              Tentang Kami
            </h3>
            <h6 class="d-flex justify-content-center text-justify">
              PT Wilwatikta Mandala Dwipantara (WM Dwipantara) merupakan Perusahaan penyedia informasi penjualan serta pelelangan aset property. Bekerja sama dengan beberapa bank besar, Kami hadir menyediakan pilihan Property dengan beragam lokasi dan harga, sehingga memudahkan anda mendapatkan rumah impian.
            </h6>
          </div>
        </div>
        
        <div class="row">
          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <h3 class="text-center">
              Benefit Yang Didapat
            </h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card card-tentang-kami py-4 px-3">
              <div class="card-body">
                <div class="d-flex justify-content-center mb-0">
                  {{-- SVG Jual Beli Rumah--}}
                    <svg class="svg-tentang-kami" width="98" height="72" viewBox="0 0 98 71" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M95.5625 67.5H88V39.375H92.6875C93.1019 39.375 93.4993 39.2104 93.7924 38.9174C94.0854 38.6244 94.25 38.2269 94.25 37.8125V33.125C94.25 32.7106 94.0854 32.3132 93.7924 32.0202C93.4993 31.7272 93.1019 31.5625 92.6875 31.5625H91.125V22.1875H92.6875C93.1019 22.1875 93.4993 22.0229 93.7924 21.7299C94.0854 21.4369 94.25 21.0394 94.25 20.625V15.9375C94.25 15.5231 94.0854 15.1257 93.7924 14.8327C93.4993 14.5397 93.1019 14.375 92.6875 14.375H44.25V1.87504C44.2503 1.62231 44.1892 1.37329 44.0721 1.14935C43.955 0.925398 43.7853 0.733203 43.5775 0.589244C43.3698 0.445286 43.1303 0.353861 42.8795 0.322812C42.6287 0.291762 42.3741 0.322014 42.1375 0.410974L4.6375 14.4735C4.33977 14.5854 4.08331 14.7856 3.90238 15.0472C3.72145 15.3088 3.62468 15.6195 3.625 15.9375V37.8125C3.625 38.2269 3.78962 38.6244 4.08265 38.9174C4.37567 39.2104 4.7731 39.375 5.1875 39.375H9.875V67.5H2.0625C1.6481 67.5 1.25067 67.6646 0.957646 67.9577C0.66462 68.2507 0.5 68.6481 0.5 69.0625C0.5 69.4769 0.66462 69.8744 0.957646 70.1674C1.25067 70.4604 1.6481 70.625 2.0625 70.625H95.5625C95.9769 70.625 96.3743 70.4604 96.6674 70.1674C96.9604 69.8744 97.125 69.4769 97.125 69.0625C97.125 68.6481 96.9604 68.2507 96.6674 67.9577C96.3743 67.6646 95.9769 67.5 95.5625 67.5ZM88 31.5625H48.9375V22.1875H88V31.5625ZM91.125 17.5V19.0625H47.375C46.9606 19.0625 46.5632 19.2272 46.2701 19.5202C45.9771 19.8132 45.8125 20.2106 45.8125 20.625V33.125C45.8125 33.5394 45.9771 33.9369 46.2701 34.2299C46.5632 34.5229 46.9606 34.6875 47.375 34.6875H91.125V36.25H44.25V17.5H91.125ZM6.75 17.0203L41.125 4.12972V36.25H6.75V17.0203ZM17.6875 67.5V45.625H22.375V67.5H17.6875ZM25.5 67.5V45.625H30.1875V67.5H25.5ZM33.3125 67.5V45.625H38V67.5H33.3125ZM41.125 67.5V45.625H45.8125V67.5H41.125ZM48.9375 67.5V44.0625C48.9375 43.6481 48.7729 43.2507 48.4799 42.9577C48.1868 42.6646 47.7894 42.5 47.375 42.5H16.125C15.7106 42.5 15.3132 42.6646 15.0201 42.9577C14.7271 43.2507 14.5625 43.6481 14.5625 44.0625V67.5H13V39.375H84.875V67.5H48.9375Z" fill="#F1E982"/>
                    </svg>
                  {{-- SVG Jual Beli Rumah end --}}
                </div>
                <h5 class="card-title text-center">
                  Jual Beli Rumah
                </h5>
                <p class="card-text text-justify">
                  Ada banyak property dari Asset bank yang terdaftar di situs kami. Sehingga anda mendapatkan pilihan Property dengan lokasi dan harga yang beragam.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card card-tentang-kami py-4 px-3">
              <div class="card-body">
                <div class="d-flex justify-content-center mb-0">
                  {{-- SVG Efektif dan Efesien --}}
                    <svg class="svg-tentang-kami" width="98" height="72" viewBox="0 0 98 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M91.125 18.8125H88V6.3125C87.9978 4.65556 87.3387 3.06711 86.167 1.89548C84.9954 0.723848 83.4069 0.0646705 81.75 0.0625H16.125C14.468 0.064361 12.8793 0.72344 11.7076 1.89514C10.5359 3.06684 9.87686 4.65547 9.875 6.3125V34.4375H6.75C5.09297 34.4394 3.50434 35.0984 2.33264 36.2701C1.16094 37.4418 0.501861 39.0305 0.5 40.6875V65.6875C0.501861 67.3445 1.16094 68.9332 2.33264 70.1049C3.50434 71.2766 5.09297 71.9356 6.75 71.9375H22.375C24.0319 71.9353 25.6204 71.2762 26.792 70.1045C27.9637 68.9329 28.6228 67.3444 28.625 65.6875V61H42.248L41.4668 64.125H36.4375C36.0231 64.125 35.6257 64.2896 35.3326 64.5826C35.0396 64.8757 34.875 65.2731 34.875 65.6875V70.375C34.875 70.7894 35.0396 71.1868 35.3326 71.4799C35.6257 71.7729 36.0231 71.9375 36.4375 71.9375H91.125C92.7819 71.9353 94.3704 71.2762 95.542 70.1045C96.7137 68.9329 97.3728 67.3444 97.375 65.6875V25.0625C97.3728 23.4056 96.7137 21.8171 95.542 20.6455C94.3704 19.4738 92.7819 18.8147 91.125 18.8125ZM25.5 65.6875C25.4989 66.516 25.1693 67.3102 24.5835 67.896C23.9977 68.4818 23.2035 68.8114 22.375 68.8125H6.75C5.92153 68.8114 5.12731 68.4818 4.54149 67.896C3.95567 67.3102 3.62609 66.516 3.625 65.6875V40.6875C3.62609 39.859 3.95567 39.0648 4.54149 38.479C5.12731 37.8932 5.92153 37.5636 6.75 37.5625H8.74805L9.95508 41.1816C10.0588 41.4928 10.2578 41.7634 10.5238 41.9551C10.7899 42.1469 11.1095 42.25 11.4375 42.25H17.6875C18.0155 42.25 18.3351 42.1469 18.6012 41.9551C18.8672 41.7634 19.0662 41.4928 19.1699 41.1816L20.377 37.5625H22.375C23.2035 37.5636 23.9977 37.8932 24.5835 38.479C25.1693 39.0648 25.4989 39.859 25.5 40.6875V65.6875ZM12.043 37.5625H17.082L16.5605 39.125H12.5645L12.043 37.5625ZM28.625 57.875V53.1875H56.75V57.875H28.625ZM56.75 61V64.125H56.4082L55.627 61H56.75ZM53.1855 64.125H44.6895L45.4707 61H52.4043L53.1855 64.125ZM38 68.8125V67.25H56.9473C57.0903 67.7976 57.3071 68.3233 57.5918 68.8125H38ZM56.75 25.0625V50.0625H28.625V40.6875C28.6228 39.0306 27.9637 37.4421 26.792 36.2705C25.6204 35.0988 24.0319 34.4397 22.375 34.4375H13V6.3125C13.0011 5.48403 13.3307 4.68981 13.9165 4.10399C14.5023 3.51817 15.2965 3.18859 16.125 3.1875H81.75C82.5785 3.18859 83.3727 3.51817 83.9585 4.10399C84.5443 4.68981 84.8739 5.48403 84.875 6.3125V18.8125H63C61.3431 18.8147 59.7546 19.4738 58.583 20.6455C57.4113 21.8171 56.7522 23.4056 56.75 25.0625ZM94.25 65.6875C94.2489 66.516 93.9193 67.3102 93.3335 67.896C92.7477 68.4818 91.9535 68.8114 91.125 68.8125H63C62.1715 68.8114 61.3773 68.4818 60.7915 67.896C60.2057 67.3102 59.8761 66.516 59.875 65.6875V25.0625C59.8761 24.234 60.2057 23.4398 60.7915 22.854C61.3773 22.2682 62.1715 21.9386 63 21.9375H91.125C91.9535 21.9386 92.7477 22.2682 93.3335 22.854C93.9193 23.4398 94.2489 24.234 94.25 25.0625V65.6875Z" fill="#F1E982"/>
                    </svg>
                  {{-- End Of SVG Efektif dan Efesien --}}
                </div>
                <h5 class="card-title text-center">
                  Efektif Dan Efesien
                </h5>
                <p class="card-text text-justify">
                  Dengan menggunakan platform kami, maka akan memudahkan property anda untuk dapat dijangkau luas oleh target pasar.
                </p>
              </div>
            </div>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <div class="card card-tentang-kami py-3 px-3">
              <div class="card-body">
                <div class="d-flex justify-content-center mb-0">
                  {{-- SVG Transaksi Terpercaya --}}
                    <svg class="svg-tentang-kami" width="98" height="72" viewBox="0 0 101 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <g clip-path="url(#clip0)">
                      <path d="M79.8959 61.6035C77.9334 57.5119 70.9125 51.8326 64.1293 46.3451C60.3459 43.2867 56.7709 40.3951 54.4918 38.116C53.9209 37.541 53.0668 37.3576 52.3127 37.6285C50.9086 38.1369 50.0418 38.5285 49.3586 38.8326C48.317 39.2992 47.967 39.4576 46.5627 39.5992C45.9418 39.6617 45.3836 39.9951 45.0377 40.5117C42.0918 44.9158 39.0418 44.5408 37.0043 43.8576C36.3543 43.641 36.2377 43.3617 36.1752 43.0951C35.7377 41.266 37.9336 37.0076 40.7793 34.1617C47.5502 27.3867 51.0418 25.6908 58.4168 28.9867C66.7834 32.7283 75.1668 35.6576 75.2502 35.6867C76.3461 36.0658 77.5252 35.4908 77.9043 34.4033C78.2793 33.3158 77.7084 32.1283 76.6209 31.7492C76.5375 31.7201 68.3209 28.8492 60.1209 25.1783C50.5668 20.9074 45.325 23.7199 37.8375 31.2117C34.9875 34.0617 31.1166 39.8283 32.1166 44.0576C32.5457 45.8576 33.8125 47.191 35.6957 47.8117C40.4166 49.3701 44.7248 47.8783 47.9332 43.6201C49.2582 43.416 49.9791 43.116 51.0623 42.6285C51.4539 42.4535 51.9123 42.2494 52.5207 42.0035C54.9041 44.241 58.1207 46.8451 61.5082 49.5826C67.6332 54.5367 74.5791 60.1535 76.1416 63.4035C76.9125 65.0076 76.0832 66.0535 75.5207 66.5326C74.6957 67.241 73.5666 67.466 72.9541 67.0367C72.2791 66.5742 71.4 66.5451 70.7041 66.9617C70.0041 67.3783 69.6125 68.1658 69.7 68.9742C69.8416 70.2992 68.6291 71.0576 68.1041 71.3201C66.7707 71.9951 65.3791 71.8785 64.8582 71.3951C64.2748 70.8535 63.4373 70.6951 62.6957 70.9701C61.9541 71.2492 61.4373 71.9326 61.3623 72.7242C61.2373 74.0908 60.2248 75.4033 58.9039 75.9158C58.2664 76.1574 57.3414 76.3033 56.5123 75.5449C55.9957 75.0783 55.2789 74.899 54.6082 75.0615C53.9291 75.2281 53.3832 75.7199 53.1457 76.374C53.0666 76.5824 52.8873 77.0824 50.9373 77.0824C49.5498 77.0824 47.0539 76.1449 45.8332 75.3365C44.3707 74.374 35.1998 67.5324 27.2791 60.899C26.1666 59.9615 24.2416 57.9531 22.5416 56.1781C21.0332 54.6031 19.6541 53.174 18.9457 52.574C18.0582 51.824 16.7457 51.9365 16.0082 52.8199C15.2666 53.699 15.3748 55.0158 16.2541 55.7574C16.9 56.3033 18.15 57.624 19.5291 59.0615C21.3875 61.0031 23.3082 63.0115 24.6 64.0949C32.3584 70.5908 41.6834 77.6033 43.5416 78.824C45.075 79.8324 48.3791 81.249 50.9375 81.249C52.9916 81.249 54.5709 80.7781 55.6666 79.8574C57.1332 80.4283 58.8 80.4324 60.4041 79.8074C62.3 79.074 63.8582 77.5949 64.7416 75.8033C66.3832 76.1533 68.2625 75.9074 69.9707 75.0492C71.6416 74.2076 72.8498 72.9201 73.4498 71.4158C75.1082 71.5033 76.8164 70.9158 78.2289 69.7033C80.6209 67.6576 81.2791 64.4744 79.8959 61.6035Z" fill="#F1E982"/>
                      <path d="M42.6041 27.0826H23.8541C22.7041 27.0826 21.7707 28.016 21.7707 29.166C21.7707 30.316 22.7041 31.2494 23.8541 31.2494H42.6041C43.7541 31.2494 44.6875 30.316 44.6875 29.166C44.6875 28.016 43.7541 27.0826 42.6041 27.0826Z" fill="#F1E982"/>
                      <path d="M86 57.1702C85.3541 56.2161 84.0625 55.9536 83.1084 56.6036L77.0168 60.6951C76.0627 61.3367 75.8084 62.6326 76.4502 63.5867C76.8543 64.1826 77.5127 64.5076 78.1836 64.5076C78.5795 64.5076 78.9836 64.3951 79.342 64.1535L85.4336 60.0619C86.3875 59.4201 86.6416 58.1244 86 57.1702Z" fill="#F1E982"/>
                      <path d="M73.0666 67.1119C71.575 65.9369 64.9041 59.1078 60.7791 54.8078C59.9832 53.9744 58.6625 53.9453 57.8332 54.7453C56.9998 55.5412 56.9748 56.8619 57.7707 57.6912C58.8416 58.8078 68.2873 68.6537 70.4832 70.3871C70.8623 70.6871 71.3207 70.833 71.7707 70.833C72.3832 70.833 72.9957 70.558 73.4123 70.0371C74.125 69.1369 73.9709 67.8244 73.0666 67.1119Z" fill="#F1E982"/>
                      <path d="M64.7416 71.291C62.2457 69.2951 55.975 62.6326 54.5582 61.091C53.7748 60.241 52.4623 60.191 51.6123 60.966C50.7664 61.7451 50.7082 63.066 51.4873 63.9119C51.5623 63.991 59.0457 72.0703 62.1373 74.5453C62.5207 74.8494 62.9832 74.9994 63.4373 74.9994C64.0457 74.9994 64.6539 74.7285 65.0664 74.2203C65.7834 73.3201 65.6375 72.0076 64.7416 71.291Z" fill="#F1E982"/>
                      <path d="M56.4501 75.487C53.4792 72.9829 47.4 66.512 46.2125 65.2411C45.425 64.3995 44.1041 64.3536 43.2666 65.1411C42.425 65.9286 42.3832 67.2452 43.1666 68.087C44.875 69.9161 50.6917 76.0829 53.7626 78.6745C54.1542 79.0036 54.6292 79.1661 55.1042 79.1661C55.6958 79.1661 56.2876 78.912 56.7001 78.4245C57.4417 77.5411 57.3292 76.2286 56.4501 75.487Z" fill="#F1E982"/>
                      <path d="M25.2875 23.4869C21.7166 20.1035 7.4375 19.0076 3.1459 18.7535C2.5543 18.7244 2.0084 18.9201 1.5918 19.316C1.175 19.7076 0.9375 20.2576 0.9375 20.8326V58.3326C0.9375 59.4826 1.8709 60.416 3.0209 60.416H15.5209C16.4209 60.416 17.2209 59.8369 17.5 58.9785C17.8041 58.0451 24.9834 36.0119 25.9291 25.1785C25.9834 24.5451 25.75 23.9201 25.2875 23.4869ZM14 56.2494H5.1041V23.066C11.8 23.5951 19.025 24.7451 21.65 26.0201C20.5459 35.1451 15.55 51.3576 14 56.2494Z" fill="#F1E982"/>
                      <path d="M98.8541 22.916C82.4957 22.916 73.4207 27.1035 73.0416 27.2785C72.5 27.5326 72.0957 28.0035 71.9291 28.5744C71.7625 29.1453 71.8457 29.7578 72.1582 30.266C74.7332 34.4119 82.7957 57.4369 84.3457 63.0535C84.5957 63.9576 85.4166 64.5826 86.3541 64.5826H98.8541C100.004 64.5826 100.937 63.6492 100.937 62.4992V24.9992C100.937 23.8451 100.004 22.916 98.8541 22.916ZM96.7709 60.416H87.9125C85.9375 53.9826 80.1041 37.2076 76.85 30.2744C80.0291 29.1953 86.8584 27.341 96.7709 27.1078V60.416Z" fill="#F1E982"/>
                      </g>
                      <defs>
                      <clipPath id="clip0">
                      <rect width="100" height="100" fill="white" transform="translate(0.9375)"/>
                      </clipPath>
                      </defs>
                    </svg>
                    
                  {{-- SVG Transaksi Terpercaya End --}}
                </div>
                <h5 class="card-title text-center">
                  Transaksi Terpercaya
                </h5>
                <p class="card-text text-justify">
                  Semua proses transaksi terverifikasi dan ditangani oleh tim profesional, sehingga memberikan rasa aman & nyaman untuk semua pihak.
                </p>
              </div>
            </div>
          </div>
        </div>


      </div>
    </div>

@endsection

@push('script')
  <script>
      $(document).ready(function(){
        
        /*JS way for setting height: 100vh to slides' height*/
        /*const $slides = $(".owl-carousel .owl-slide");
        $slides.css("height", $(window).height());
        $(window).resize(() => {
        $slides.css("height", $(window).height());
        });*/

        $(".owl-carousel").on("initialized.owl.carousel", () => {
        setTimeout(() => {
            $(".owl-item.active .owl-slide-animated").addClass("is-transitioned");
            $("section").show();
          }, 200);
        });

        const $owlCarousel = $(".owl-carousel").owlCarousel({
          items: 1,
          loop: true,
          autoplay:true,
          autoplayTimeout:5000,
          autoplayHoverPause:true,
          nav: false,
          navText: [
              '<svg width="50" height="50" viewBox="0 0 24 24"><path d="M16.67 0l2.83 2.829-9.339 9.175 9.339 9.167-2.83 2.829-12.17-11.996z"/></svg>',
              '<svg width="50" height="50" viewBox="0 0 24 24"><path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z"/></svg>' /* icons from https://iconmonstr.com */
            ]
        });

        $owlCarousel.on("changed.owl.carousel", e => {
          $(".owl-slide-animated").removeClass("is-transitioned");

          const $currentOwlItem = $(".owl-item").eq(e.item.index);
          $currentOwlItem.find(".owl-slide-animated").addClass("is-transitioned");

          const $target = $currentOwlItem.find(".owl-slide-text");
          doDotsCalculations($target);
        });

        $owlCarousel.on("resize.owl.carousel", () => {
        setTimeout(() => {
            setOwlDotsPosition();
          }, 50);
        });

        /*if there isn't content underneath the carousel*/
        //$owlCarousel.trigger("refresh.owl.carousel");

        setOwlDotsPosition();

        function setOwlDotsPosition() {
          const $target = $(".owl-item.active .owl-slide-text");
          doDotsCalculations($target);
        }

        function doDotsCalculations(el) {
          const height = el.height();
          const {top, left} = el.position();
          const res = height + top + 20;

          $(".owl-carousel .owl-dots").css({
              top: `${res}px`,
              left: `${left}px`
          });
        }

      });
  </script>
@endpush
    