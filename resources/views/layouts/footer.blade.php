<link rel="stylesheet" href="{{ asset('css/footer-style.css') }}">
    {{-- content footer --}}
        <section class="footer-container">

          <div class="container py-5 px-3">
            <div class="row mb-3">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h3>
                  Kontak Kami
                </h3>
              </div>
            </div>
            {{-- first section --}}
            <div class="row">
              <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-left">
                    <i class="fas fa-home pr-2"></i>
                    <p>
                      <a href="https://goo.gl/maps/eyP2hna3La6JE5tr9" target="_blank">
                        Jl. Belimbing 3 Blok I no. 24 RT/RW. 02/21 Pancoran Mas – Kota Depok 16431
                      </a>
                    </p>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-left">
                    <i class="fas fa-phone pr-2"></i>
                    <p>
                      (021) 7773420
                      <br>
                      087887833666
                    </p>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-left">
                    <i class="fas fa-envelope pr-2"></i>
                    <p>
                      <a href="mailto:wmdwipantara@gmail.com" target="_blank">
                        wmdwipantara@gmail.com
                      </a>
                    </p>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-left">
                    <i class="fas fa-globe pr-2"></i>
                    <p>
                      <a href="https://wmdwipantara.id" target="_blank">https://wmdwipantara.id</a>
                    </p>
                  </div>
                </div>
              </div>
              {{-- second section --}}
              <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="row mb-3">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-between">
                    <a href="#" target="_blank" style="font-size:12px;">
                      Petunjuk Penjualan Rumah
                    </a>
                  </div>
                </div>  

                <div class="row mb-3">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-between">
                    <a href="#" target="_blank" style="font-size:12px;">
                      Petunjuk Pembelian Rumah
                    </a>
                  </div>
                </div>  

                <div class="row mb-3">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-between">
                    <a href="#" target="_blank" style="font-size:12px;">
                      Petunjuk Pemasangan Iklan
                    </a>
                  </div>
                </div>  

                <div class="row mb-3">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-between">
                    <a href="#" target="_blank" style="font-size:12px;">
                      Petunjuk Penggunaan Aplikasi
                    </a>
                  </div>
                </div>  

                <div class="row">
                  <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-between">
                    <a href="#" target="_blank" style="font-size:12px;">
                      Petunjuk Transaksi
                    </a>
                  </div>
                </div>  
              </div>
              {{-- third section --}}
              <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
                <div class="">
                  <a href="#" target="_blank"> 
                    <img class="img-fluid mx-auto d-block" src="{{ asset('images/Playstore.png') }}" alt="Playstore" width="300">
                  </a>
                </div>
              </div>
            </div>

          </section>

        <section class="footer-copyright py-3 px-3 text-white" style="font-size: 12px !important;background-color:#333 !important;">
          <div class="container-fluid">
            <div class="row">

              {{-- <div class="d-flex justify-content-start"> --}}
                <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 my-2">
                  <a href="#">
                    <span class="text-white">Kebijakan Privasi</span>
                  </a>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 my-2">
                  <a href="#">
                    <span class="text-white">Ketentuan Layanan</span>
                  </a>
                </div>
                <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 my-2">
                  <a href="#">
                    <span class="text-white">Persyaratan Pembelian</span>
                  </a>
                </div>
              {{-- </div> --}}
              <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 my-2" style="font-size: 12px !important;">
                <a href="http://wmdwipantara.id/" target="_blank">
                  <span class="text-white">&copy; 2020 PT. Wilwatikta Mandala Dwipantara</span>
                </a>
              </div>
            </div>
          </div>
        </section>
    {{-- end of content footer --}}

    <script src="{{ asset('js/jquery-3.5.1.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="https://kit.fontawesome.com/86bd5d84df.js" crossorigin="anonymous"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
    @stack('script')
    <script>
      $(document).ready(function(){
        // Preeloader
        $(".preloader").fadeOut();
      });
    </script>
  </body>
</html>