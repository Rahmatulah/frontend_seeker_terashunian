<link rel="stylesheet" href="{{ asset('css/style-navbar.css') }}">
<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:white;">
    <div class="container">
        <a class="navbar-brand d-flex justify-content-left" href="{{ url('/') }}">
            <img src="{{ asset('images/WM.jpg') }}" alt="WM Dwipantara" width="60" height="30">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse d-flext justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav" style="color:#8d8f91;">
            <li class="nav-item px-3">
            <a class="nav-link" href="{{ url('beli') }}"><span>Beli</span></a>
            </li>
            <li class="nav-item px-3">
            <a class="nav-link " href="{{ url('lelang') }}"><span class="{{request()->is('/beli')? 'text' : ''}}">Lelang</span></a>
            </li>
            <li class="nav-item px-3">
            <a class="nav-link " href="#">
                <span>
                    Simulasi KPR
                </span>
            </a>
            </li>
            <li class="nav-item px-3">
                <a class="nav-link btn btn-md btn-default" href="#" style="background-color: #F1E982;">
                    <span>Jual Sekarang</span>
                </a>
            </li>
        </ul>
        </div>
    </div>
</nav>