{{-- @push('css') --}}
  <link rel="stylesheet" href="{{ asset('css/searching.css') }}">
{{-- @endpush --}}
<div style="margin-top:100px;"></div>
<div class="container mt-5 mb-5">
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card-searching px-5 pt-5 pb-3">

        {{-- searcing --}}
          <form>
            <div class="row">
              <div class="input-group col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <input class="form-control form-control-xl" type="text" placeholder="Cari berdasarkan lokasi atau area" style="height: 9vh;">
                <div class="input-group-prepend">
                  <button type="button" class="input-group-text btn btn-default" id="basic-addon1" style="background-color:#F1E982;">
                      <i class="fas fa-search"></i>
                  </button>
                </div>
            </div>
              <div class="row" style="font-size:10px !important;">

                <div class="col-md-4 col-lg-3 col-xl-3">
                  <div class="dropdown">
                      <a class="btn btn-muted dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dekat Dari
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                  </div>
                </div>

                <div class="col-md-4 col-lg-3 col-xl-3">
                  <div class="dropdown">
                      <a class="btn btn-muted dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Harga
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                  </div>
                </div>

                <div class="col-md-4 col-lg-3 col-xl-3">
                  <div class="dropdown">
                      <a class="btn btn-muted dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Luas Tanah
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                  </div>
                </div>

                <div class="col-md-4 col-lg-3 col-xl-3">
                  <div class="dropdown">
                      <a class="btn btn-muted dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Luas Bangunan
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                      </div>
                  </div>
                </div>

                
              </div>
            </div>
          </form>
        {{-- end of searching --}}

      </div>
    </div>
  </div>
</div>
