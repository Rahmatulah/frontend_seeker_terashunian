@extends('layouts.main')
@section('title', "Listing Rumah")
<link rel="stylesheet" href="{{ asset('css/style-beli.css') }}">
@section('content')
    <div class="container py-5">
        <h6 class="text-secondary">
            <i>
                {{ request()->is('beli')? "Beranda / Pencarian" : '' }}
            </i>
        </h6>
        <div class="result-text">
            <div class="row">
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <h3 class="d-flex justify-content-start">
                        Hasil dari pencarian "Real Estate"
                    </h3>
                </div>
                <div class="col-sm-5 col-md-5 col-lg-5 col-xl-5">
                    <h6 class="d-flex justify-content-end text-secondary text-right" style="font-size:14px;">
                        Tampil 12 dari 589
                    </h6>
                </div>
            </div>
        </div>
        <div class="card-iklan-highlight">

            <div class="row">

                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card item-highlight rounded-top rounded-bottom">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <img src="{{ asset('images/Featured.png') }}" alt="Featured" style="position: absolute;top:0;left;">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card item-highlight rounded-top rounded-bottom">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <img src="{{ asset('images/Featured.png') }}" alt="Featured" style="position: absolute;top:0;left;">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card item-highlight rounded-top rounded-bottom">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <img src="{{ asset('images/Featured.png') }}" alt="Featured" style="position: absolute;top:0;left;">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card item-highlight rounded-top rounded-bottom">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <img src="{{ asset('images/Featured.png') }}" alt="Featured" style="position: absolute;top:0;left;">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="card-result-listing">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card list-card">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card list-card">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card list-card">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 my-2 pt-0">
                    <div class="card list-card">
                        <img class="card-img-top" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/owlcarousel2.jpg" alt="Card image cap">
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Go somewhere</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <button type="button" class="btn btn-outline-secondary py-2 px-2" style="width: 100% !important;">
                        Tampilkan Lebih Banyak
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection